# Portuguese translation for mailman debconf messages
# Copyright (C) 2007 Miguel Figueiredo
# This file is distributed under the same license as the mailman package.
# Miguel Figueiredo <elmig@debianpt.org>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: mailman 2.1.9-5\n"
"Report-Msgid-Bugs-To: mailman@packages.debian.org\n"
"POT-Creation-Date: 2011-10-08 15:13+0000\n"
"PO-Revision-Date: 2008-12-28 10:31+0000\n"
"Last-Translator: Miguel Figueiredo <elmig@debianpt.org>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "ar (Arabic)"
msgstr "ar (Arábe)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "ca (Catalan)"
msgstr "ca (Catalão)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "cs (Czech)"
msgstr "cs (Checo)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "da (Danish)"
msgstr "da (Dinamarquês)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "de (German)"
msgstr "de (Alemão)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "en (English)"
msgstr "en (Inglês)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "es (Spanish)"
msgstr "es (Espanhol)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "et (Estonian)"
msgstr "et (Estónio)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "eu (Basque)"
msgstr "eu (Basco)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "fi (Finnish)"
msgstr "fi (Finlandês)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "fr (French)"
msgstr "fr (Françês)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "hr (Croatian)"
msgstr "hr (Croata)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "hu (Hungarian)"
msgstr "hu (Húngaro)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "ia (Interlingua)"
msgstr "ia (Interlíngua)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "it (Italian)"
msgstr "it (Italiano)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "ja (Japanese)"
msgstr "ja (Japonês)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "ko (Korean)"
msgstr "ko (Coreano)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "lt (Lithuanian)"
msgstr "lt (Lituano)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "nl (Dutch)"
msgstr "nl (Holandês)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "no (Norwegian)"
msgstr "no (Norueguês)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "pl (Polish)"
msgstr "pl (Polaco)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "pt (Portuguese)"
msgstr "pt (Português)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "pt_BR (Brasilian Portuguese)"
msgstr "pt_BR (Português do Brasil)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "ro (Romanian)"
msgstr "ro (Romeno)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "ru (Russian)"
msgstr "ru (Russo)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "sl (Slovenian)"
msgstr "sl (Esloveno)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "sr (Serbian)"
msgstr "sr (Sérvio)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "sv (Swedish)"
msgstr "sv (Sueco)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "tr (Turkish)"
msgstr "tr (Turco)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "uk (Ukrainian)"
msgstr "uk (Ucraniano)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "vi (Vietnamese)"
msgstr "vi (Vietnamita)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "zh_CN (Chinese - China)"
msgstr "zh_CN (Chinês - China)"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "zh_TW (Chinese - Taiwan)"
msgstr "zh_TW (Chinês - Formosa)"

#. Type: multiselect
#. DefaultChoice
#. You must NOT translate this string, but you can change its value.
#. The comment between brackets is used to distinguish this msgid
#. from the one in the Choices list; you do not have to worry about
#. them, and have to simply choose a msgstr among the English values
#. listed in the Choices field above, e.g. msgstr "nl (Dutch)".
#. Type: select
#. DefaultChoice
#. You must NOT translate this string, but you can change its value.
#. The comment between brackets is used to distinguish this msgid
#. from the one in the Choices list; you do not have to worry about
#. them, and have to simply choose a msgstr among the English values
#. listed in the Choices field above, e.g. msgstr "nl (Dutch)".
#: ../templates:1002 ../templates:4001
msgid "en (English)[ default language ]"
msgstr "pt (Portuguese)"

#. Type: multiselect
#. Description
#: ../templates:1003
msgid "Languages to support:"
msgstr "Idiomas a suportar:"

#. Type: multiselect
#. Description
#: ../templates:1003
msgid ""
"For each supported language, Mailman stores default language specific texts "
"in /etc/mailman/LANG/ giving them conffile like treatment with the help of "
"ucf.  This means approximately 150kB for each supported language on the root "
"file system."
msgstr ""
"Para cada idioma suportado, o Mailman guarda os textos específicos do idioma "
"por omissão em /etc/mailman/LANG/ dando-lhes um tratamento tipo conffile com "
"a ajuda do ucf.  Isto significa aproximadamente 150kB para cada idioma "
"suportado no sistema de ficheiros raiz."

#. Type: multiselect
#. Description
#: ../templates:1003
msgid ""
"If you need a different set of languages at a later time, just run dpkg-"
"reconfigure mailman."
msgstr ""
"Se você necessitar de um conjunto diferente de idiomas noutra altura, apenas "
"execute dpkg-reconfigure mailman."

#. Type: multiselect
#. Description
#: ../templates:1003
msgid ""
"NOTE: Languages enabled on existing mailing lists are forcibly re-enabled "
"when deselected and mailman needs at least one language for displaying its "
"messages."
msgstr ""
"NOTA: Os idiomas habilitados em listas de correio existentes são "
"forçadamente re-habilitados quando os deseleccionar e o mailman necessita de "
"pelo menos um idioma para mostrar estas mensagens."

#. Type: note
#. Description
#: ../templates:3001
msgid "Missing site list"
msgstr "Falta lista de site"

#. Type: note
#. Description
#: ../templates:3001
msgid ""
"Mailman needs a so-called \"site list\", which is the list from which "
"password reminders and such are sent out from.  This list needs to be "
"created before mailman will start."
msgstr ""
"O Mailman necessita do chamado \"site list\", que é a lista a partir da qual "
"os alertas de palavra-chave são enviados.  Esta lista necessita ser criada "
"antes do mailman iniciar."

#. Type: note
#. Description
#: ../templates:3001
msgid ""
"To create the list, run \"newlist mailman\" and follow the instructions on-"
"screen.  Note that you also need to start mailman after that, using "
"service mailman start."
msgstr ""
"Para criar a lista, corra \"newlist mailman\" e siga as instruções no ecrã. "
"Note que após isso também tem de iniciar o mailman, utilizando service "
"mailman start."

#. Type: select
#. Description
#: ../templates:4002
msgid "Default language for Mailman:"
msgstr "Idioma por omissão para o Mailman:"

#. Type: select
#. Description
#: ../templates:4002
msgid ""
"The web page will be shown in this language, and in general, Mailman will "
"use this language to communicate with the user."
msgstr ""
"A página web será mostrada neste idioma, e em geral, o Mailman irá utilizar "
"este idioma para comunicar com o utilizador."

#. Type: select
#. Choices
#: ../templates:5001
msgid "abort installation"
msgstr "abortar a instalação"

#. Type: select
#. Choices
#: ../templates:5001
msgid "continue regardless"
msgstr "continuar de qualquer forma"

#. Type: select
#. Description
#: ../templates:5002
msgid "Old queue files present"
msgstr "Estão presentes ficheiros da fila antiga"

#. Type: select
#. Description
#: ../templates:5002
msgid ""
"The directory /var/lib/mailman/qfiles contains files. It needs to be empty "
"for the upgrade to work properly. You can try to handle them by:\n"
" - Stop new messages from coming in (at the MTA level).\n"
" - Start a mailman queue runner: service mailman start\n"
" - Let it run until all messages are handled.\n"
"   If they don't all get handled in a timely manner, look at the logs\n"
"   to try to understand why and solve the cause.\n"
" - Stop it: service mailman stop\n"
" - Retry the upgrade.\n"
" - Let messages come in again.\n"
"You can also decide to simply remove the files, which will make Mailman "
"forget about (and lose) the corresponding emails."
msgstr ""
"O directório /var/lib/mailman/qfiles contém ficheiros. Necessita estar vazio "
"para a actualização funcionar correctamente. Você pode lidar com eles "
"assim:\n"
" - Parar novas mensagens de entrar (ao nível do MTA).\n"
" - Iniciar uma execução da fila do mailman: service mailman start\n"
" - Deixa-lo correr até as mensagens serem lidadas.\n"
"   Se não forem todos lidados a tempo, veja os logs para tentar perceber\n"
"   porquê e resolver a causa.\n"
" - Para-lo: service mailman stop\n"
" - Tentar novamente a actualização.\n"
" - Deixar as mensagens entrar novamente.\n"
"Pode também decidir simplesmente remover os ficheiros, o que fará o Mailman "
"esquecer acerca (e perder) os mails correspondentes."

#. Type: select
#. Description
#: ../templates:5002
msgid ""
"If these files correspond to shunted messages, you have to either delete "
"them or unshunt them (with /var/lib/mailman/bin/unshunt). Shunted messages "
"are messages on which Mailman has already abandoned any further processing "
"because of an error condition, but that are kept for admin review. You can "
"use /var/lib/mailman/bin/show_qfiles to examine the contents of the queues."
msgstr ""
"Se estes ficheiros correspondem a mensagens 'shunted', você tem de apagá-las "
"ou fazer unshunt (com /var/lib/mailman/bin/unshunt). Mensagens 'shunted' são "
"mensagens nas quais o Mailman já abandonou qualquer processamento por causa "
"de uma condição de erro, mas são mantidas para revisão pelo administrador. "
"Você pode utilizar var/lib/mailman/bin/show_qfiles para examinar o conteúdo "
"das filas."

#. Type: select
#. Description
#: ../templates:5002
msgid ""
"You have the option to continue installation regardless of this problem, at "
"the risk of losing the messages in question or breaking your Mailman setup."
msgstr ""
"Tem a opção de continuar a instalação independentemente deste problema, com "
"o risco de perder as mensagens em questão ou estragar a configuração do seu "
"Mailman."

#~ msgid "Gate news to mail?"
#~ msgstr "Redireccionar as news para o e-mail?"

#~ msgid ""
#~ "Mailman allows to gate news to mail, that is, send all the messages which "
#~ "appear in a Usenet newsgroup to a mailing list."
#~ msgstr ""
#~ "O Mailman permite redireccionar as news para o e-mail, isto é, enviar "
#~ "todas as mensagens que aparecem num newsgroup Usenet para a lista de "
#~ "correio."

#~ msgid "Most people won't need this."
#~ msgstr "A maioria das pessoas não necessitará disto."
